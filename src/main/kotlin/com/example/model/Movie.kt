package com.example.model

import kotlinx.serialization.Serializable

@Serializable
data class Movie(val id:String, var title:String, var year:Int, var genre:String, var director:String) {
    var comments = mutableListOf<Comment>()
}

val moviesStorage = mutableListOf<Movie>()