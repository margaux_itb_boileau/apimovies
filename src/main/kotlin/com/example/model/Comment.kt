package com.example.model

import kotlinx.serialization.Serializable
import java.util.Date

@Serializable
data class Comment(val id:String, val idMovie:String, val comment:String, var date: String = Date().toString())