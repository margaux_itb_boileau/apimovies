package com.example.routes

import com.example.model.Comment
import com.example.model.Movie
import com.example.model.moviesStorage
import io.ktor.http.*
import io.ktor.server.application.*
import io.ktor.server.request.*
import io.ktor.server.response.*
import io.ktor.server.routing.*

fun Route.moviesRouting() {
    route("/movies") {
        //all
        get {
            if (moviesStorage.isNotEmpty()) call.respond(moviesStorage)
            else call.respondText("No movies found", status = HttpStatusCode.OK)
        }
        //id
        get("{id?}") {
            if (call.parameters["id"].isNullOrBlank()) return@get call.respondText(
                "Missing id",
                status = HttpStatusCode.BadRequest
            )
            val id = call.parameters["id"]
            for (movie in moviesStorage) {
                if (movie.id == id) return@get call.respond(movie)
            }
            call.respondText("No movies with id $id", status = HttpStatusCode.NotFound)
        }
        //id/comments
        get("{id?}/comments") {
            if (call.parameters["id"].isNullOrBlank()) return@get call.respondText(
                "Missing id",
                status = HttpStatusCode.BadRequest
            )
            val id = call.parameters["id"]
            for (movie in moviesStorage) {
                if (movie.id == id) {
                    if (movie.comments.isNotEmpty()) return@get call.respond(movie.comments)
                    else return@get call.respondText("No comments found", status = HttpStatusCode.OK)
                }
            }
            call.respondText("No movies with id $id", status = HttpStatusCode.NotFound)
        }
        //add
        post {
            val movie = call.receive<Movie>()
            moviesStorage.add(movie)
            call.respondText("Movie stored correctly", status = HttpStatusCode.Created)
        }
        //id/add
        post("{id?}") {
            if (call.parameters["id"].isNullOrBlank()) return@post call.respondText(
                "Missing id",
                status = HttpStatusCode.BadRequest
            )
            val id = call.parameters["id"]
            val commentToAdd = call.receive<Comment>()
            for (movie in moviesStorage) {
                if (movie.id == id) {
                    movie.comments.add(commentToAdd)
                    return@post call.respond(movie)
                }
            }
            call.respondText("No movies with id $id", status = HttpStatusCode.NotFound)
        }
        //update/id
        put("{id?}") {
            if (call.parameters["id"].isNullOrBlank()) return@put call.respondText(
                "Missing id",
                status = HttpStatusCode.BadRequest
            )
            val id = call.parameters["id"]
            val movieToUpd = call.receive<Movie>()
            for (movie in moviesStorage) {
                if (movie.id == id) {
                    movie.title = movieToUpd.title
                    movie.year = movieToUpd.year
                    movie.genre = movieToUpd.genre
                    movie.director = movieToUpd.director
                    movie.comments = movieToUpd.comments
                    return@put call.respond(movie)
                }
            }
            call.respondText("No movies with id $id", status = HttpStatusCode.NotFound)
        }
        //delete/id
        delete("{id?}") {
            if (call.parameters["id"].isNullOrBlank()) return@delete call.respondText(
                "Missing id",
                status = HttpStatusCode.BadRequest
            )
            val id = call.parameters["id"]
            for (movie in moviesStorage) {
                if (movie.id == id) {
                    moviesStorage.remove(movie)
                    return@delete call.respondText("Movie removed correctly", status = HttpStatusCode.Accepted)
                }
            }
            call.respondText(
                "Movie with id $id not found",
                status = HttpStatusCode.NotFound
            )

        }
    }
}
